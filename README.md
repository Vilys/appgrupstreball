Gestió de grups de treball d'estudiants mitjançant db4o
=======================================================
L’objectiu d’aquesta activitat és veure com gestionar la persistència d’objectes usant la BDOO db4o.
Es disposa de les dues classes `Estudiant` i `GrupTreball`, el codi de les quals es mostra tot seguit. A partir d’aquest codi, es desitja crear una aplicació que, mitjançant una BDOO db4o gestioni a quins grups de treball pertanyen diferents estudiants dins una escola. Donat un grup de treball, aquest pot tenir assignats diversos estudiants, però tot estudiant només té un (però sempre un) únic grup de treball. L’aplicació ha de poder fer el següent:

* Donar d’alta un nou estudiant. A l’hora d’assignar-li un grup, si el nom indicat no existeix, es crea un nou grup. Si existeix, a l’estudiant se li assigna aquell grup.
* Reassignar un estudiant a un altre grup de treball. Aquest grup ja ha d’existir. Si, en fer-ho, el grup antic queda sense membres, cal esborrar-lo de la BDOO.
* Llistar tots els grups existents.
* Llistar tots els estudiants (i a quin grup pertanyen).

Es considera que els noms dels grups i dels estudiants són únics al sistema. No hi pot haver noms repetits. En base a la descripció, també cal remarcar que l’única manera de crear grups nous és afegint-hi nous estudiants.

Per dur a terme aquesta tasca, el codi font d’aquestes dues classes no es pot modificar en absolut.
