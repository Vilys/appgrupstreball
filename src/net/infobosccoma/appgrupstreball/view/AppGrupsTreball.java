package net.infobosccoma.appgrupstreball.view;

import java.util.Iterator;
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;
import java.util.Scanner;
import net.infobosccoma.appgrupstreball.model.Estudiant;
import net.infobosccoma.appgrupstreball.model.GrupTreball;

/**
 * Classe que conté el mètode principal de l'aplicació de gestió d'estudiants i 
 * grups de treball.
 * 
 * L'aplicació ha de gestionar, mitjançant una BDOO db4o, a quins grups de treball 
 * pertanyen diferents estudiants dins una escola. Donat un grup de treball, 
 * aquest pot tenir assignats diversos estudiants, però tot estudiant només en té 
 * un (però sempre un) únic grup de treball. L’aplicació ha de poder fer el 
 * següent:
 *     - Donar d’alta un nou estudiant. A l’hora d’assignar-li un grup, 
 *       si el nom indicat no existeix, es crea un nou grup. Si existeix, a 
 *       l’estudiant se li assigna aquell grup.
 *     - Reassignar un estudiant a un altre grup de treball. Aquest grup ja ha 
 *       d’existir. Si, en fer-ho, el grup antic queda sense membres, cal 
 *       esborrar-lo de la BDOO.
 *     - Llistar tots els grups existents.
 *     - Llistar tots els estudiants (i a quin grup pertanyen).
 * 
 * Es considera que els noms dels grups i dels estudiants són únics al sistema. <-No hem queda clar si el control l'he d fer jo i s'asumeix que serà aixi, he asumit que seria aixi.
 * No hi pot haver noms repetits. En base a la descripció, també cal remarcar 
 * que l’única manera de crear grups nous és afegint-hi nous estudiants.
 * 
 * @author Marti Vilares Castello
 */
public class AppGrupsTreball {
    private static ObjectContainer db = Db4oEmbedded.openFile("appGrupsTreball.db4o");//He configurat el projecte de netbeans perque treballi dins la carpeta "files" del projecte
    static Scanner teclat = new Scanner(System.in);
    /**
     * Tanca la conexió amb la BD un cop es tanca el programa
     * 
     * @param args 
     */
    public static void main(String[] args) {
	try {
	    gMenu();
	} finally {
            db.close();
        }
    }
    /**
     * Genera el menu principal
     * 
     */
    private static void gMenu() {
	System.out.println("Escolleizi la opció que vol dur a terme");
	System.out.println("\t 1-Donar d'alta un estudiant");
	System.out.println("\t 2-Canviar el grup d'un estudiant");
	System.out.println("\t 3-Llistar tots el grups");
	System.out.println("\t 4-Llistar tots els estudiants (i a quin grup pertanyen)");
	System.out.println("\t 0-Sortir");
	
	switch(Byte.parseByte(teclat.nextLine())) {
	    case 1:
		altaEstudiant();
		break;
	    case 2:
		canviarGrupEstudiant();
		break;
	    case 3:
		llistarGrups();
		break;
	    case 4:
                llistarEstudiants();
		break;
	    case 0:
		break;
	    default:
		System.out.println("Opció incorrecte introdueixi un dels numeros del menu.");
		gMenu();
		break;
	}
    }
    /**
     * Permet afegir un ou estudiant i un nou grup en cas de que el grup que volem assignar a l'estudiant no existeixi
     * 
     */
    private static void altaEstudiant() {
	System.out.println("Introdeixi el nom del nou estudiant:");
	String nom = teclat.nextLine();
	System.out.println("Introdeixi el grup del nou estudiant:");
	String grup = teclat.nextLine();
	if (comprobarGrup(grup)) {
	    db.store(new Estudiant(nom, getGrupTreball(grup)));
	} else {
	    System.out.println("El grup especificat no exiteix, introdueixi la descripció del nou grup:");
	    String descGrup = teclat.nextLine();
	    db.store(new GrupTreball(grup, descGrup));
	    db.store(new Estudiant(nom, getGrupTreball(grup)));
	    System.out.println("");
	}
	System.out.println("");
	gMenu();
    }
    /**
     * Permet canviar el grup de l'estudiant
     * 
     */
    private static void canviarGrupEstudiant() {
	System.out.println("Introdueixi el nom de l'estudiant que vol canviar de grup:");
	String nom = teclat.nextLine();
	if (comprobarEstudiant(nom)) {
	    System.out.println("Introdueixi el nou grup de l'estudiant (el grup ha d'existir):");
	    String grup = teclat.nextLine();
	    if (comprobarGrup(grup)) {
		Estudiant canviGrup = getEstudiant(nom);
		GrupTreball grupAntic = canviGrup.getGrupTreball();
		canviGrup.reassignaGrup(getGrupTreball(grup));
		db.store(canviGrup);
		db.store(getGrupTreball(grup));
		if (grupAntic.getNumEstudiants() == 0){
		    db.delete(grupAntic);
		}
	    } else {
		System.out.println("El grup especificat no existeix.");
	    }
	}
	System.out.println("");
	gMenu();
    }
    /**
     * Mostra tots els grups que hi hagui a la BD
     * 
     */
    private static void llistarGrups() {
	System.out.println("Llistat de tots els grups amb la seva descripció");
	System.out.println("------------------------------------------------");
	GrupTreball getAllGrups = new GrupTreball(null, null);
	mostrarResultat(db.queryByExample(getAllGrups));
	System.out.println("");
	gMenu();
    }
    /**
     * Mostra tots els estudiants que hi hagui a la BD
     * 
     */
    private static void llistarEstudiants() {
	System.out.println("Llistat de tots els estudiants i el grup al que pertanyen");
	System.out.println("---------------------------------------------------------");
	GrupTreball grupTmp = new GrupTreball(null, null);
	Estudiant getAllEstudiants = new Estudiant(null, grupTmp);
	getAllEstudiants.getGrupTreball().restaEstudiant();
	mostrarResultat(db.queryByExample(getAllEstudiants));
	System.out.println("");
	gMenu();
    }
    /**
     * Mostra per pantalla els objectes del Set que rep
     * 
     * @param result Set dels objectes a mostrar
     */
    private static void mostrarResultat(ObjectSet result) {
	Iterator it = result.iterator();
	while (it.hasNext()) {
	    System.out.println(it.next());
	}
    }
    /**
     * Comprova si el grup amb el nom indicat 
     * 
     * @param grup nom del grup a buscar
     * @return true si el troba alramentt false
     */
    private static boolean comprobarGrup(String grup) {
	GrupTreball getGrup = new GrupTreball(grup, null);
	return !db.queryByExample(getGrup).isEmpty();
    }
    /**
     * Retorna el grup de treball amb el nom indicat
     * 
     * @param grup nom del grup a buscar
     * @return 
     */
    private static GrupTreball getGrupTreball(String grup) {
	GrupTreball getAllGrups = new GrupTreball(grup, null);
	return (GrupTreball) db.queryByExample(getAllGrups).get(0);
    }
    /**
     * Comprova si l'estudiant amb el nom entrat existeix i que només hi es un cop 
     * 
     * 
     * @param nom nom de l'estudiant
     * @return true = si que existei un cop sino false
     */
    private static boolean comprobarEstudiant(String nom) {
	Predicate query = new Predicate<Estudiant>() {
		@Override
		public boolean match(Estudiant e) {
		    return nom.equalsIgnoreCase(e.getNom());
		}
	    };
	ObjectSet<Estudiant> result = db.query(query);
	
	return result.size() == 1;
    }
    /**
     * Retorna l'estudiant amb el nom indicat
     * 
     * @param nom nom de l'estudiant
     * @return 
     */
    private static Estudiant getEstudiant(String nom) {
	Predicate query = new Predicate<Estudiant>() {
		@Override
		public boolean match(Estudiant e) {
		    return nom.equalsIgnoreCase(e.getNom());
		}
	    };
	ObjectSet<Estudiant> result = db.query(query);
	
	if (result.size() == 1) {
	    return result.next();
	} else {
	    return null;
	}
    }
}
    
