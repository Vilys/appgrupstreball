package net.infobosccoma.appgrupstreball.model;

/**
 * Classe que modela un estudiant que té un nom i pertany a un grup de treball
 * 
 * @author marc
 */
public class Estudiant {

    //<editor-fold defaultstate="collapsed" desc="Atributs membre">
    private String nom;
    private GrupTreball grup;
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="API pública">

    //<editor-fold defaultstate="collapsed" desc="Constructor">    
    public Estudiant(String nom, GrupTreball grup) {
        this.nom = nom;
        this.grup = grup;
        this.grup.sumaEstudiant();
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Getters/Setters">    
    public String getNom() {
        return nom;
    }
    
    public GrupTreball getGrupTreball() {
        return grup;
    }
    //</editor-fold>
    
    /**
     * Canvia el grup de l'estudiant, actualitzant la quantitat d'estudiants
     * del grup vell i del nom grup
     * 
     * @param grup El nou grup de l'estudiant 
     */
    public void reassignaGrup(GrupTreball grup) {
        if (this.grup != grup) {
            this.grup.restaEstudiant();
            this.grup = grup;
            this.grup.sumaEstudiant();
        }        
    }
    
    //<editor-fold defaultstate="collapsed" desc="Mètodes sobreescrits">    
    
    @Override
    public String toString() {
        return nom + " -> " + grup;
    }
    //</editor-fold>
    
   //</editor-fold>
}
